terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.69.0"
    }
  }
}
provider "aws" {
  region = "us-east-1"
  access_key = "AKIA3O5DIYSORF7LLQ4O"
  secret_key = "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"
}

# create the VPC
resource "aws_vpc" "My_VPC1" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = true
  enable_dns_hostnames = true

tags = {
    Name = "My VPC"
}
}
# create the Subnet
resource "aws_subnet" "My_VPC_Subnet_01" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
tags = {
   Name = "My VPC Subnet_01"
}
}
resource "aws_subnet" "My_VPC_Subnet_02" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
tags = {
   Name = "My VPC Subnet_02"
}
}

resource "aws_subnet" "My_VPC_Subnet_03" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
tags = {
   Name = "My VPC Subnet_03"
}
}

resource "aws_subnet" "My_VPC_Subnet_04" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
tags = {
   Name = "My VPC Subnet_04"
}
}

resource "aws_subnet" "My_VPC_Subnet_05" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"
tags = {
   Name = "My VPC Subnet_05"
}
}

resource "aws_subnet" "My_VPC_Subnet_06" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.6.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
tags = {
   Name = "My VPC Subnet_06"
}
}

resource "aws_subnet" "My_VPC_Subnet_07" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.7.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
tags = {
   Name = "My VPC Subnet_07"
}
}

resource "aws_subnet" "My_VPC_Subnet_08" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.8.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
tags = {
   Name = "My VPC Subnet_08"
}
}
resource "aws_subnet" "My_VPC_Subnet_09" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.9.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
tags = {
   Name = "My VPC Subnet_09"
}
}
resource "aws_subnet" "My_VPC_Subnet_10" {
  vpc_id                  = aws_vpc.My_VPC1.id
  cidr_block              = "10.0.10.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"
tags = {
   Name = "My VPC Subnet_10"
}
}


resource "aws_internet_gateway" "My_VPC_GW" {
 vpc_id = aws_vpc.My_VPC1.id
 tags = {
        Name = "My VPC Internet Gateway"
}
}
resource "aws_route_table" "My_VPC_route_table_Public" {
 vpc_id = aws_vpc.My_VPC1.id
route {
    cidr_block = "0.0.0.0/10"
    gateway_id = aws_internet_gateway.My_VPC_GW.id
  }
 tags = {
        Name = "My VPC Route Table"
}
}
resource "aws_security_group" "allow_tls" {
  vpc_id      = aws_vpc.My_VPC1.id
  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 65535
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.My_VPC1.cidr_block]
    
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}
resource "aws_security_group" "webserver" {
  vpc_id      = aws_vpc.My_VPC1.id
  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.My_VPC1.cidr_block]
    
  }
  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  } 
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "webserver"
  }
}

resource "aws_route_table_association" "My_VPC_association1" {
  subnet_id      = aws_subnet.My_VPC_Subnet_01.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_route_table_association" "My_VPC_association4" {
  subnet_id      = aws_subnet.My_VPC_Subnet_04.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_route_table_association" "My_VPC_association6" {
  subnet_id      = aws_subnet.My_VPC_Subnet_06.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_route_table_association" "My_VPC_association8" {
  subnet_id      = aws_subnet.My_VPC_Subnet_08.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_route_table_association" "My_VPC_association9" {
  subnet_id      = aws_subnet.My_VPC_Subnet_09.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_route_table_association" "My_VPC_association10" {
  subnet_id      = aws_subnet.My_VPC_Subnet_10.id
  route_table_id = aws_route_table.My_VPC_route_table_Public.id
} # end resource

resource "aws_eip" "IP_for_bastion" {
  instance = aws_instance.bastion.id  
  vpc      = true
  tags = {
    "Name" = "IP for Bastion"
  }
}
resource "aws_eip" "webserver1" {
  instance = aws_instance.webserver1.id   
  vpc      = true
  tags = {
    "Name" = "webserver1"
  }
}
resource "aws_eip" "webserver2" {
  instance = aws_instance.webserver2.id 
  vpc      = true
  tags = {
    "Name" = "webserver2"
  }
}
resource "aws_instance" "bastion" {
  ami           = "ami-0b17e49efb8d755c3" # us-west-1
  instance_type = "t2.micro"
  subnet_id = aws_subnet.My_VPC_Subnet_01.id
  security_groups = [aws_security_group.allow_tls.id]
  tags = {
    "Name" = "Bastion host"
  }
}

resource "aws_instance" "webserver1" {
  ami           = "ami-0ed9277fb7eb570c9" # us-west-1
  instance_type = "t2.medium"
  subnet_id = aws_subnet.My_VPC_Subnet_02.id
  security_groups = [aws_security_group.webserver.id]
  
    tags = {
    "Name" = "WEb server 01"
  }
}

resource "aws_instance" "webserver2" {
  ami           = "ami-0ed9277fb7eb570c9" # us-west-1
  instance_type = "t2.medium"
  subnet_id = aws_subnet.My_VPC_Subnet_02.id
  security_groups = [aws_security_group.webserver.id]
  
  tags = {
    "Name" = "WEb server 02"
  }
}

resource "aws_s3_bucket" "b31dec2012ghjk" {
  bucket = "b31dec2012ghjk"
  acl    = "private"
  versioning {
    enabled = true
  }
   logging {
    target_bucket = aws_s3_bucket.n31dec2012ghjktg.id
    target_prefix = "log/"
  }
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket" "n31dec2012ghjktg" {
  bucket = "n31dec2012ghjktg"
  acl    = "private"
  versioning {
    enabled = true
  }
  tags = {
    Name        = "My bucket1"
    Environment = "Dev"
 }
}

